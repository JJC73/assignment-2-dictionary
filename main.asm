%include "lib.inc"
%include "colon.inc"
%include "words.inc"

%define BYTE 8
%define WRITE 1
%define ZERO 0
%define MAX_LENGTH 256
extern find_word
section .bss
        buffer: times MAX_LENGTH db 0
section .rodata
        maxLenBuffError: db "Error: Max length of Buffer", 10
        nofFoundElement: db "Error: The key does not exist", 10
section .text
global _start:
_start:
        mov rdi, buffer
        mov rsi, MAX_LENGTH
        call read_word
        test rax, rax
        jz .errorMaxLen
        mov rdi, rax
        mov rsi, NEXT_ELEMENT
        call find_word
        cmp rax, ZERO
        jz .errorNotFound
        mov rdi, rax
        call print_element
        jmp .stop
        .errorNotFound:
                mov rdi , nofFoundElement
                mov rdx, rax
                mov rax, WRITE
                mov rdi, 2
                mov rsi, nofFoundElement
                syscall
                call print_newline
                jmp .stop
        .errorMaxLen:
                mov rdi , maxLenBuffError
                mov rdx, rax
                mov rax, WRITE
                mov rdi, 2
                mov rsi, maxLenBuffError
                syscall
                call print_newline
                jmp .stop
        .stop:
                call exit
print_element:
        add rdi, BYTE
        lea rdi,[rdi+rdx+1]
        call print_string
        call print_newline
        sub rdi, BYTE
        ret
