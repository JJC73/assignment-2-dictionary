global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

exit:
    xor rax, rax,
    mov rax, 60
    syscall

string_length:
    xor rax, rax
    .loop:
        mov cl, [rdi+rax]
        test cl, cl
        jz .exit
        inc rax
        jmp .loop
    .exit:
    ret
print_string:
    xor rax, rax
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall

    ret
print_char:

    xor rax, rax
    push rdi
    mov rsi , rsp
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    pop rdi
    ret
print_newline:
    xor rax, rax
    mov rdi, 0xA
   call print_char
    ret
print_uint:
    xor rax, rax
    mov rax, rdi
    mov r8, 10
    mov r9, rsp
    push 0
    .loop:
        xor rdx, rdx
        div r8
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        jz .end
        jmp .loop
    .end:
        mov rdi, rsp
        call print_string
            mov rsp, r9

        ret
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .pos

    .neg:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
        ret
    .pos:
        call print_uint
        ret
string_equals:
    xor rax, rax
    xor rcx, rcx
    .loop:
        mov r9b, [rdi+rcx]
        mov r10b, [rsi+rcx]
        cmp r9b, 0
        jz .isfalse
        cmp r10b, 0
        jz .isfalse
        cmp r9b, r10b
        jnz .isfalse
        inc rcx
        jmp .loop
    .isfalse:
        cmp r9b, r10b
        jz .istrue
        mov rax, 0
        ret
    .istrue:
        mov rax, 1
        ret
read_char:
    xor rax, rax
    push 0
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

read_word:
;rdi - buff
;rsi - maxlen
        xor rax, rax
        xor rcx, rcx
        .loop:
                push rdi
                push rsi
                push rcx
                call read_char
                pop rcx
                pop rsi
                pop rdi

                cmp rax, 0
                jz .end

                cmp rax, 0x20
                jz .space_char
                cmp rax, 0x9
                jz .space_char
                cmp rax, 0xA
                jz .space_char

                cmp rcx, rsi
                jge .maxlen


                mov [rdi+rcx], rax
                inc rcx
                 jmp .loop
        .space_char:
                cmp rcx, 0
                jz .loop
                jmp .end

        .maxlen:
                xor rax, rax
                xor rdx, rdx
                ret

        .end:
                mov byte[rdi+rcx], 0
                mov rax, rdi
                mov rdx, rcx
                ret
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r9, 10
    .loop:
        xor rdx, rdx
         mov r8b, byte[rdi+rcx]
         cmp r8b, 0
         je .end
        cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end
         sub r8b, '0'
        mul r9
        mov dl, r8b
        add rax, rdx
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        ret
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    jz .neg
    .pos:
        call parse_uint
        ret
    .neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret
string_copy:
    xor rax, rax
    xor rcx, rcx
    .loop:
        cmp rcx, rdx
        jz .maxlen
        mov r9b, byte[rdi+rcx]
        mov byte[rsi+rcx], r9b
        cmp r9b, 0
        inc rcx
        jz .end
        jmp .loop
    .maxlen:
        mov rax, 0
        ret
    .end:
        mov rax, rcx
        ret
