%include "lib.inc"
%define BYTE 8
global find_word
find_word:
        .loop:
                cmp rsi, 0
                jz .stop
                add rsi, BYTE
                mov r12, rsi
                mov r13, rdi
                call string_equals
                mov rdi, r13
                mov rsi, r12
                sub rsi, BYTE
                cmp rax, 1
                jz .found
                mov rsi, [rsi]
                jmp .loop
        .found:
                mov rax, rsi
                ret
        .stop:
                xor rax, rax
                ret
