%macro colon 2
        %ifdef NEXT_ELEMENT
        %2:dq NEXT_ELEMENT
        %else
        %2: dq 0
        %endif
        db %1, 0
        %define NEXT_ELEMENT %2
%endmacro
