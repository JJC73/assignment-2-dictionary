ASM=nasm
ASMFLAGS=-f elf64

.PHONY: clean

program: main.o dict.o  lib.o
        ld -o program $^

clean:
        rm -rf *.o
%.o: %.asm
        $(ASM) $(ASMFLAGS) -o $@ $<
